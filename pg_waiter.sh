#!/usr/bin/env sh

usage() {
cat <<EOF
This script waits until postgresql enter ready state.

EOF
exit 1
}

main() {
    if [ $# -lt 2 ]; then
        usage
    fi

    echo Waiting for postgresql;

    until pg_isready --quiet $@;
    do sleep 2;
    done;

    echo Postgresql is up;

    exit 0
}

main "$@"
